#include <systemc.h>
#include "ALU_CTRL.hpp"

using namespace std;
using namespace sc_core;

void ALU_CTRL::behav()
{
   sc_bv<3> temp;
   while(true)
   {
   	 wait();
 	 temp[0] = ALU_Op.read()[1] & (funct.read()[3] | funct.read()[0]);
	 temp[1] = (~ALU_Op.read()[1]) | (~funct.read()[2]);
	 temp[2] = ALU_Op.read()[0] | (ALU_Op.read()[1] & funct.read()[1]);

     Operation->write(temp);
   }
}