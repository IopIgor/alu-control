#ifndef ALU_CTRL_HPP
#define ALU_CTRL_HPP

#include <systemc.h>
#include <iostream>

using namespace std;
using namespace sc_core;

SC_MODULE(ALU_CTRL) 
{
  sc_in<sc_bv<2> > ALU_Op;
  sc_in<sc_bv<6> > funct;
  sc_out<sc_bv<3> > Operation;
  
  SC_CTOR(ALU_CTRL) 
  {
    SC_THREAD(behav);
      sensitive << ALU_Op << funct;
  } 
  
 private:
  void behav();
};

#endif
