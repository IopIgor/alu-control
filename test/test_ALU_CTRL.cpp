#include <systemc.h>
#include <iostream>
#include "ALU_CTRL.hpp"

using namespace std;
using namespace sc_core;

const sc_time wait_time(5, SC_NS);
const unsigned numb_test = 5;
const sc_bv<3> theor_res[numb_test] = {"111", "101", "110", "110", "110"};

SC_MODULE(TestBench) 
{
 public:
  sc_signal<sc_bv<2> > ALU_Op;
  sc_signal<sc_bv<6> > funct;
  sc_signal<sc_bv<3> > Operation;

  ALU_CTRL test_alu_ctrl;

  SC_CTOR(TestBench) : test_alu_ctrl("test_alu_ctrl")
  {
    SC_THREAD(stimulus_thread);
    
    test_alu_ctrl.ALU_Op(this->ALU_Op);
    test_alu_ctrl.funct(this->funct);
    test_alu_ctrl.Operation(this->Operation);
  }

  bool check() 
  {
    bool error = 0;
    for(unsigned i = 0; i < numb_test; i++)
    {
      if(theor_res[i] != results[i])
      {
        cout << "test failed!!\nTheoric result : " << theor_res[i]
             << "\nMy result: " <<  results[i] << endl << endl;
        error = 1;
      }
    }
    return error;
  }
 
 private:
  sc_bv<3> results[numb_test];

  void stimulus_thread() 
  {    
    unsigned i = 0;

    ALU_Op.write("10");
    funct.write("010011");
    wait(wait_time);
    cout << "At time " << sc_time_stamp() << ": the ALU control returns: " 
         << Operation.read() << endl;
    results[i] = Operation.read();

    funct.write("011110");
    wait(wait_time);
    cout << "At time " << sc_time_stamp() << ": the ALU control returns: " 
         << Operation.read() << endl;
    results[++i] = Operation.read();

    ALU_Op.write("01");
    wait(wait_time);
    cout << "At time " << sc_time_stamp() << ": the ALU control returns: " 
         << Operation.read() << endl;
    results[++i] = Operation.read();

    funct.write("110000");
    wait(wait_time);
    cout << "At time " << sc_time_stamp() << ": the ALU control returns: " 
         << Operation.read() << endl;
    results[++i] = Operation.read();

    ALU_Op.write("11");
    wait(wait_time);
    cout << "At time " << sc_time_stamp() << ": the ALU control returns: " 
         << Operation.read() << endl;
    results[++i] = Operation.read();
  }
};

int sc_main(int argc, char** argv) 
{
  TestBench test("test");

  sc_start();

  return test.check();
}
